$(document).ready(function(){

    $(".btn-menu").on('click', function(event){

        event.preventDefault();
        var value = $(this).attr('data-filter');
        $(".btn-menu").removeClass("active");
        // $(".tab").addClass("active"); // instead of this do the below 
        $(this).addClass("active"); 
        
        if(value == "all")
        {

            $('.filter').show(1000);
        }
        else
        {

            $(".filter").not('.'+value).fadeOut("slow");
            $('.filter').filter('.'+value).fadeIn("slow");

            
        }
    });

});